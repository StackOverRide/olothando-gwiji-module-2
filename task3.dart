class App {
  String? name;
  String? category;
  String? developer;
  int? year;
  App(String name, String category, String developer, int year) {
    this.name = name;
    this.category = category;
    this.developer = developer;
    this.year = year;
    print("App name: $name");
    print("Category: $category");
    print("Developer: $developer");
    print("Year: $year");
  }
  void transform() {
    String transformedname = "$name".toUpperCase();
    print(transformedname);
  }
}

void main() {
  App app =
      new App("Ambani Africa", "Educational", "info@ambaniafrica.com", 2021);
  print("App name in Capital letters: ");
  app.transform();
}
